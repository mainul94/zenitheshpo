# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from . import __version__ as app_version

app_name = "zenith_eshop"
app_title = "Zenith Eshop"
app_publisher = "Mainul Islam"
app_description = "EShop"
app_icon = "fa fa-shop"
app_color = "brown"
app_email = "mainulkhan94@gmail.com"
app_license = "MIT"

# Includes in <head>
# ------------------

# include js, css files in header of desk.html
# app_include_css = "/assets/zenith_eshop/css/zenith_eshop.css"
# app_include_js = "/assets/zenith_eshop/js/zenith_eshop.js"

# include js, css files in header of web template
# web_include_css = "/assets/zenith_eshop/css/zenith_eshop.css"
# web_include_js = "/assets/zenith_eshop/js/zenith_eshop.js"

# include js in page
# page_js = {"page" : "public/js/file.js"}

# include js in doctype views
doctype_js = {
    "Woocommerce Settings": "public/js/woo_setting.js"
    }
# doctype_list_js = {"doctype" : "public/js/doctype_list.js"}
# doctype_tree_js = {"doctype" : "public/js/doctype_tree.js"}
# doctype_calendar_js = {"doctype" : "public/js/doctype_calendar.js"}

# Home Pages
# ----------

# application home page (will override Website Settings)
# home_page = "login"

# website user home page (by Role)
# role_home_page = {
#	"Role": "home_page"
# }

# Website user home page (by function)
# get_website_user_home_page = "zenith_eshop.utils.get_home_page"

# Generators
# ----------

# automatically create page for each record of this doctype
# website_generators = ["Web Page"]

# Installation
# ------------

# before_install = "zenith_eshop.install.before_install"
# after_install = "zenith_eshop.install.after_install"

# Desk Notifications
# ------------------
# See frappe.core.notifications.get_notification_config

# notification_config = "zenith_eshop.notifications.get_notification_config"

# Permissions
# -----------
# Permissions evaluated in scripted ways

# permission_query_conditions = {
# 	"Event": "frappe.desk.doctype.event.event.get_permission_query_conditions",
# }
#
# has_permission = {
# 	"Event": "frappe.desk.doctype.event.event.has_permission",
# }

# Document Events
# ---------------
# Hook on document methods and events

# doc_events = {
# 	"Item": {
# 		"before_insert": "zenith_eshop.utils.item.make_barcode_number"
# 	}
# }

# Scheduled Tasks
# ---------------

# scheduler_events = {
# 	"all": [
# 		"zenith_eshop.tasks.all"
# 	],
# 	"daily": [
# 		"zenith_eshop.tasks.daily"
# 	],
# 	"hourly": [
# 		"zenith_eshop.tasks.hourly"
# 	],
# 	"weekly": [
# 		"zenith_eshop.tasks.weekly"
# 	]
# 	"monthly": [
# 		"zenith_eshop.tasks.monthly"
# 	]
# }

# Testing
# -------

# before_tests = "zenith_eshop.install.before_tests"

# Overriding Whitelisted Methods
# ------------------------------
#
override_whitelisted_methods = {
	"erpnext.erpnext_integrations.connectors.woocommerce_connection.order": "zenith_eshop.utils.woocommerce_connection.order"
}

