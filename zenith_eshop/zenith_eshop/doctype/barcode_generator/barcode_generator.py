# -*- coding: utf-8 -*-
# Copyright (c) 2018, Techbeeo and contributors
# For license information, please see license.txt

from __future__ import unicode_literals
import frappe
from frappe.model.document import Document
from frappe.model.mapper import get_mapped_doc
from frappe.utils import flt


class BarcodeGenerator(Document):
    pass


@frappe.whitelist()
def get_standard_selling_price(item_code):
    item_rate, template = frappe.db.get_value("Item", {"item_code": item_code}, ["standard_rate as rate", "variant_of"])
    if item_rate:
        return item_rate
    else:
        price_list = frappe.get_single("Selling Settings")
        item_rate = frappe.db.get_value("Item Price", {"item_code": item_code, "price_list": price_list.selling_price_list},
                                   'price_list_rate as rate')
        if not item_rate and template:
            return get_standard_selling_price(template)
        return item_rate

@frappe.whitelist()
def make_barcode_printable(source_name, target_doc=None):
    def update_item(obj, target, source_parent):
        item = frappe.get_doc("Item", target.item_code)
        target.group_name = item.item_group
        target.barcode_number = item.barcode
        target.item_price = get_standard_selling_price(target.item_code).get('rate', 0)

    doclist = get_mapped_doc("Purchase Invoice", source_name, {
        "Purchase Invoice": {
            "doctype": "Barcode Generator",
            "validation": {
                "docstatus": ["=", 1]
            }
        },
        "Purchase Invoice Item": {
            "doctype": "Barcode Generator Items",
            "postprocess": update_item,
            "field_map": {
                "qty": "quantity"
            }
        }
    }, target_doc)

    return doclist


@frappe.whitelist()
def get_items_from_temp(item_code):
    rows = frappe.get_all("Item", {"variant_of": item_code}, ['name as item_code', 'item_name', 'item_group'])
    for row in rows:
        row['barcode_number'] = frappe.get_value("Item Barcode", {"parent": row.item_code}, "barcode")
        row['item_price'] = get_standard_selling_price(row.item_code)
    return rows