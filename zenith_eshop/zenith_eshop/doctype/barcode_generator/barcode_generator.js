// Copyright (c) 2018, Techbeeo and contributors
// For license information, please see license.txt
let isMadeBarcode = false;

frappe.ui.form.on('Barcode Generator', {
	setup: function(frm) {
		frm.add_fetch('item_code', 'item_group', 'group_name');
		frappe.require("assets/zenith_eshop/js/JsBarcode.all.min.js")
       },
	refresh: frm => {
        frm.add_custom_button(__("Template Item"), function () {
			frm.events.get_from_purchase_invoice()
        }, __("Get"));
	},
	get_from_purchase_invoice: () => {
        let dialog = new frappe.ui.Dialog({
			title: __("Get Item from Item Template"),
			fields: [
				{
					fieldtype: "Link",
					fieldname: "item",
					options: "Item",
					get_query: function() {
						return {
							filters: {
								has_variants: 1
							}
						}
					},
					label: __("Template")
				}
			]
		});
        dialog.set_primary_action(__("get"), function (values) {
        	frappe.call({
				method: "zenith_eshop.zenith_eshop.doctype.barcode_generator.barcode_generator.get_items_from_temp",
				args: {
					item_code: values.item,
					fields: ['name', 'item_name', 'item_group']
				},
				callback: function (r) {
					if (!r.xhr) {
						r.message.forEach(function(item){
							let row = cur_frm.add_child('barcode_items', item)
						})
						cur_frm.refresh_field('barcode_items')
					}
				}
			});
			dialog.hide()
        });
        dialog.show()
	},
	generate: function (frm) {
		let $wrapper = frm.fields_dict.barcode_html.$wrapper;
		let items = frm.doc.barcode_items;
		$wrapper.html("");
		let inc = 0;
		$.each(items, function (data) {

			for(let i = 0; i < parseInt(items[data].quantity); i++){
			let name = items[data].item_code;
			let group = items[data].group_name;
			let price = items[data].item_price;
			let barcode_no = items[data].barcode_number;

			$wrapper.append(`<div class='printlayout'>
				<span>${name}</span><svg id="barcode${inc}"></svg>
				<span class="price">TK: ${price} (Incl.Vat)</span></div>`);
			JsBarcode("#barcode"+inc, barcode_no, {
				width:1.2,
                height:25,
                format: "EAN13"
            });
			inc++
			}
        });
		frm.set_value('barcode_html', $wrapper.html());
		console.log(items)
		isMadeBarcode = true;
    },
	print_barcode: function () {
		if(isMadeBarcode) {
            let custome_print = $('<div>').addClass('only_print').prependTo($('body'));
            custome_print.html($('div[data-fieldname="barcode_html"]').html());
            $('body *:not(.only_print, .only_print *)').hide();
            window.print();
            window.location.reload()
        } else{
			frappe.msgprint("Make Barcode first!");
		}
    }
});

frappe.ui.form.on('Barcode Generator Items', 'item_code', function (frm, cdt, cdn) {
	let row = locals[cdt][cdn];
	if(row.item_code) {
		frappe.call({
			method:"frappe.client.get_value",
			args: {
				doctype: "Item Barcode",
				filters: {
					parenttype: "Item",
					parent: row.item_code
				},
				parent: row.item_code,
				fieldname: 'barcode'
			},
			freeze: true,
			callback: function(data) {
				if(data['message']){
					row.barcode_number = data.message.barcode;
					cur_frm.refresh_field('barcode_items');
				}
				if(row.barcode_number == null){
					frappe.model.remove_from_locals(row.doctype, row.name);
					cur_frm.refresh_field('barcode_items');
					frappe.throw("No barcode number found for this item!");
				}
				frappe.call({
					method: "zenith_eshop.zenith_eshop.doctype.barcode_generator.barcode_generator.get_standard_selling_price",
					args: {"item_code": row.item_code},
					callback: function (r) {
						if (r.message) {
							frappe.model.set_value(cdt, cdn, 'item_price', r.message)
						}
					}
				})
			}
		})
	}
});

