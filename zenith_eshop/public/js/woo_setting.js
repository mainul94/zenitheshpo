frappe.ui.form.on("Woocommerce Settings", {
    refresh: function(frm) {
        frm.add_custom_button(__("Push Items"), ()=>{
            frappe.call({
                method: "zenith_eshop.utils.woocommerce_methods.push_items"
            })
        })
    }
})
