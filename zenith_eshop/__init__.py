# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from erpnext.stock.doctype.item.item import Item
from types import MethodType
from .utils.item import get_ean13
import frappe

__version__ = '0.0.1'

class UpdateItem(Item):
	def make_barcode_number(self):
		if self.get("__islocal") and not self.has_variants and self.get('barcodes'):
			self.set('barcodes', None)
		if not (self.get('barcodes') or len(self.barcodes)):
			barcode = get_ean13()
			barcodes = [{
				"barcode": barcode
			}]
			self.set('barcodes', barcodes)
		super(Item, self).validate()

Item.validate = UpdateItem.make_barcode_number.__func__
