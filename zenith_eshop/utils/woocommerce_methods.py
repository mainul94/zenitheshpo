import frappe
from woocommerce import API
from frappe.utils.background_jobs import enqueue

def get_wcapi():
    woocommerce_settings = frappe.get_doc("Woocommerce Settings")
    return API(
        url=woocommerce_settings.woocommerce_server_url,
        consumer_key= woocommerce_settings.api_consumer_key,
        consumer_secret= woocommerce_settings.api_consumer_secret,
        wp_api=True,
        version="wc/v3",
        query_string_auth=True
    )

@frappe.whitelist()
def push_items():
    enqueue('zenith_eshop.utils.woocommerce_methods.push_new_items', 'long', now=True)
    return True


def push_new_items(filters=None):
    if not filters: filters = {}
    filters['woocommerce_check'] = 0
    conditions, values = frappe.db.build_conditions(filters)

    sql = frappe.db.sql("""select item.item_name as name, item.name as sku, description, description as short_description, price_list_rate as regular_price from tabItem as item
    left join `tabItem Price` as ip on item.name = ip.item_code and ip.selling = 1
    where %s limit 99"""%(conditions), values, as_dict= True)
    wcapi = get_wcapi()
    response_items = wcapi.post('products/batch', { "create": sql}).json()
    for item in response_items.get('create') or []:
        if item.get('error'):
            frappe.log_error(item, item['error']['message'])
            continue
        doc = frappe.get_doc("Item", item['sku'])
        doc.db_set('woocommerce_id', item['id'])
        doc.db_set('woocommerce_check', 1)
