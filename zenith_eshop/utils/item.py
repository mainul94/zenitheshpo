import frappe

from random import randrange

def generate_12_random_numbers():
    numbers = []
    for x in range(12):
        numbers.append(randrange(10))
    return numbers

def calculate_checksum(ean):
    """
    Calculates the checksum for an EAN13
    @param list ean: List of 12 numbers for first part of EAN13
    :returns: The checksum for `ean`.
    :rtype: Integer
    """
    assert len(ean) == 12, "EAN must be a list of 12 numbers"
    sum_ = lambda x, y: int(x) + int(y)
    evensum = reduce(sum_, ean[::2])
    oddsum = reduce(sum_, ean[1::2])
    return (10 - ((evensum + oddsum * 3) % 10)) % 10

def get_ean13():
    numbers = generate_12_random_numbers()
    numbers.append(calculate_checksum(numbers))
    barcode =  ''.join(map(str, numbers))
    if frappe.db.exists("Item Barcode", {"barcode": barcode}):
        get_ean13()
    return barcode

def make_barcode_number(self):
    frappe.call("Hi")
    if self.is_new() and not self.has_variants and len(self.barcodes):
        self.set('barcodes', None)
    if not len(self.barcodes):
        barcode = get_ean13()
        barcodes = [{
            "barcode": barcode
        }]
        self.set('barcodes', barcodes)
    super(Item, self).validate()