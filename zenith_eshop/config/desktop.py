# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from frappe import _

def get_data():
	return [
		{
			"module_name": "Zenith Eshop",
			"color": "brown",
			"icon": "fa fa-shop",
			"type": "module",
			"label": _("Zenith Eshop")
		},
		{
			"module_name": "Barcode Generator",
			"color": "brown",
			"icon": "fa fa-list",
			"type": "link",
			"label": _("Barcode Generator"),
			"link": "Form/Barcode Generator/Barcode Generator"
		}
	]
